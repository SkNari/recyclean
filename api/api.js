const { default: axios } = require('axios');
const express = require('express')
const app = express()
const port = 8000

app.get('/products/:id', (req, res) => {
  var id = req.params.id;
  axios.get("https://world.openfoodfacts.org/api/v0/product/"+id).then( (response) => {

      let status = response.data.status;
      if(status == 1){
          res.status(200).json({product : response.data.product});
      }else{
        res.status(404).json({message : response.data.status_verbose});
      }
  })
})

app.get('/random/products', (req,res) => {

  res.status(200).json({message:"yes"});

})

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`)
})